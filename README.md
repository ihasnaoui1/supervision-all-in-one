# Test effectué en local :
    - Machine Ubuntu
    - dokcer 
    - docker-compose
    



1.  cloner le depot :
git clone https://gitlab.com/ihasnaoui1/supervision-all-in-one.git

2.  Des fichiers à modifier : alertmanager.yml, prometheus.yml, grafana.ini

./prometheus.yml 

./alertmanager.yml

./grafan_db/grafana.ini


=> les modification dans les diferentes fichiers sert à activer l'envoi des alertes de supervision.


3. une fois terminer: lancer 

    - docker-compose up -d
    - dokcer ps
    
```
CONTAINER ID        IMAGE                      COMMAND                  CREATED             STATUS              PORTS                    NAMES
c5a289f41b5d        google/cadvisor:latest     "/usr/bin/cadvisor -…"   23 minutes ago      Up 23 minutes       0.0.0.0:8080->8080/tcp   cadvisor
2aef7cffdcfd        prom/node-exporter         "/bin/node_exporter"     23 minutes ago      Up 23 minutes       0.0.0.0:9100->9100/tcp   node_exporter
5627482afaec        prom/prometheus:latest     "/bin/prometheus --c…"   23 minutes ago      Up 8 minutes        0.0.0.0:9090->9090/tcp   prometheus
6251e68a0667        grafana/grafana            "/run.sh"                23 minutes ago      Up 23 minutes       0.0.0.0:3000->3000/tcp   grafana
1f3d3dac875f        prom/alertmanager:latest   "/bin/alertmanager -…"   23 minutes ago      Up 10 minutes       0.0.0.0:9093->9093/tcp   alertmanager


```
    
la liste des containers avec les @IP et Ports :

```
IP              hostname        port
127.25.1.3      grafana         3000
127.25.1.4      prometheus      9090
127.25.1.5      alertmanager    9093
127.25.1.6      node_exporter   9100
127.25.1.7      cadvisor        8080

```
